# -*- coding: utf-8 -*-
"""
Created on Fri Mar  9 13:30:48 2018

@author: enovi
"""

import nltk

#objective and subjective
#count objective and subjective words in the news

def main():
    choice = True
    article_text = ''
    c = 'y'
    print('Welcome to Objective/Subjective Analyzer!')
    while choice == True:
         c = input('Analyze an article y/n? ')
         if c == 'y':
             article_text = get_text()
             segment_length = get_length()
             analyze_text(article_text,segment_length)
             choice = True
         elif c == 'n':
             choice = False

def get_text():
    article_text = ''
    article_text = str(input('Enter the message here: '))
    return article_text

def get_length():
    c = input('Split article into segments y/n? ')
    if c == 'y':
        segment_length = int(input('Enter the number of segments to split the text into: '))
    else:
        segment_length = 1
    if segment_length < 1:
        segment_length = 1
    return segment_length

def analyze_text(article_text,segment_length):
    #objective and subjective phrase lists
    obj_list    = ['accept', 'agree', 'allow', 'alter', 'announce', 'approve', 'argue',
                   'assert', 'assign', 'authority', 'begin', 'benefit', 'bill', 'brief',
                   'career', 'charity', 'comment', 'commit', 'conflict', 'consult', 'cost',
                   'court', 'data', 'day', 'deal', 'decision', 'did', 'direct', 'disclose',
                   'donate', 'drop', 'email', 'employ', 'end', 'enforce', 'engage', 'enroll',
                   'evidence', 'express', 'fact', 'fax', 'file', 'focus', 'force', 'group',
                   'halt', 'hour', 'interpret', 'joke', 'law', 'legal', 'legislation', 'limit',
                   'message', 'month', 'not', 'number', 'own', 'pact', 'paper', 'past', 'permit',
                   'platform', 'post', 'predict', 'pressure', 'previous', 'print', 'proof',
                   'publish', 'react', 'receive', 'recent', 'recipient', 'release', 'remind',
                   'report', 'request', 'respond', 'response', 'restrict', 'reveal', 'rise',
                   'said', 'say', 'send', 'sent', 'show', 'solve', 'speech', 'statistic', 'stop',
                   'term', 'time', 'today', 'tomorrow', 'treaty', 'try', 'tweet', 'veto', 'vote',
                   'write', 'year', 'yesterday']
    
    subj_list   = ['actor', 'admonish', 'afraid', 'against', 'alliance', 'ally', 'alternative',
                   'ambitious', 'anger', 'angry', 'assume', 'attempt', 'bad', 'behave', 'belief',
                   'believe', 'best', 'boss', 'bust', 'buttoned', 'care', 'claim', 'class',
                   'complain', 'concern', 'concern', 'condemn', 'conspiracy', 'conspire',
                   'controversy', 'credo', 'creed', 'creep', 'crisis', 'critic', 'dark', 'deep',
                   'denounce', 'disappoint', 'disaster', 'discord', 'dissent', 'distort', 'doom',
                   'eager', 'echo', 'edgy', 'effusive', 'embarrass', 'extreme', 'fake',
                   'fantasy', 'fastidious', 'fear', 'free', 'fundamental', 'gaffe', 'give',
                   'goal', 'good', 'great', 'greed', 'harangue', 'harm', 'harsh', 'horrible',
                   'hurt', 'hypocrisy', 'hypocrite', 'idiot', 'illusion', 'inflate', 'insult',
                   'ire', 'just', 'label', 'madman', 'malice', 'mean', 'messy', 'mislead',
                   'misled', 'moral', 'must', 'need', 'offend', 'offense', 'omen', 'ominous',
                   'ordinary', 'panic', 'power', 'private', 'propaganda', 'prove', 'public',
                   'purpose', 'rage', 'realistic', 'reality', 'reasonable', 'rid', 'right',
                   'rough', 'routine', 'ruling', 'scandal', 'scum', 'secret', 'selfish', 'shady',
                   'shall', 'shock', 'should', 'sin', 'sloppy', 'smear', 'social',
                   'sophisticate', 'spar', 'stacked', 'stage', 'stun', 'subordinate', 'support',
                   'team', 'terrible', 'trash', 'troll', 'true', 'truth', 'unable', 'undermine',
                   'valid', 'value', 'vice', 'victim', 'virtue', 'waddle', 'wealth', 'welfare',
                   'will', 'work', 'worst', 'wrong']
    
    obj_count   = 0
    subj_count  = 0
    
    article_text = article_text.lower().split()
    
    for word in article_text:
        word = nltk.PorterStemmer().stem(word)
        
    for word in obj_list:
        word = nltk.PorterStemmer().stem(word)
        
    for word in subj_list:
        word = nltk.PorterStemmer().stem(word)
        
    section = int(len(article_text) / segment_length)    
    
    for i in range(1,segment_length + 1):
        selected_text = article_text[section * (i-1):section * i]    
    
        for word in selected_text:
            if word in obj_list:
                obj_count  += 1
            elif word in subj_list:
                subj_count += 1    
        
        print('Segment {} of {}.'.format(i,segment_length))
        #display number of objective and subjective phrases
        if obj_count > 1:
            print('There were {} objective words.'.format(obj_count))
        elif obj_count == 1:
            print('There was one objective word.')
        else:
            print('There were no objective words.')
        if subj_count > 1:    
            print('There were {} subjective words.'.format(subj_count))
        elif subj_count == 1:
            print('There was one subjective word.')
        else:
            print('There were no subjective words.')
        #reset variables
        obj_count  = 0
        subj_count = 0    
        
main()